# Assignments

Below are various assignments for you to do. There's a special focus on arrays and hashes since I feel like you're not entirely comfortable with those yet, so let's try to get you used to them :) The first assignment, the music playtime program, should be your first one since it was an interview question. After that there are three more assignments that get harder in difficulty.

Did you know that my favorite gene is the Sonic Hedgehog gene? It doesn't do anything particularly cool. I just love it for its intentionally silly name. One of my goals back when I was in bio was to one day be a scientist that discovers a new gene or protein and name it something silly like that. Seeing as how I'm no longer in bio, I'm now settling for a good paying job in tech, save up money, and bribe a scientist to do that for me.
There's no real point to this story. I just thought I should fill up some space here.

## Assignment Pages

1. [Song Playtimes](https://bitbucket.org/MalpracticeLiability/assignments/src/54b6ee997cb75890e17addf099f5b1bf071848c5/SongPlaytimeCalculator/SongPlaytime.md?at=master)
	
	This should be your first assignment. It's my best recreation of last cycle's interview question. You should be able to solve this in one or two days.
	
2. [Zillow Listings](https://bitbucket.org/MalpracticeLiability/assignments/src/059c714254544afaba6e794be40628f7b970bcc5/ZillowListings/ZillowListings.md?at=master&fileviewer=file-view-default)
	
	This should be a fairly routine assignment for you. It's in line with what you've done before and should be of similar difficulty to your technical interview question. I'd expect this to take one or two days.
	
3. [Job Application Manager](https://bitbucket.org/MalpracticeLiability/assignments/src/059c714254544afaba6e794be40628f7b970bcc5/JobAppManager/JobAppManager.md?at=master&fileviewer=file-view-default)

	This is a bit harder in difficulty than you may be used to. This still covers the same concepts as before, but I'm ramping up the difficulty while challenging your problem solving abilities. It's noticeably a step up in difficulty compared to previous programs. This will be a test of your versatility and creative ability in utillizing the data structures you already know. In my opinion this is a good benchmark for where you should be by this time next week. Therefore if it feels too hard, don't get frustrated because it definitely is a challenging one. At your current level, I'd expect this to take about five days.
	
4. [Luke's Diner](https://bitbucket.org/MalpracticeLiability/assignments/src/059c714254544afaba6e794be40628f7b970bcc5/LukesDiner/LukesDiner.md?at=master&fileviewer=file-view-default)
	
	This is a Gilmore Girls themed assignment!
	
	This is also the hardest assignment you're going to get for a long time from me. The assignment will challenge your ability to keep track of complex, continuously-changing data structures. So far all your data structures have been relatively static in that once you create and add things, it stays there. With this program you'll have to delete things on the fly and move data from one structure to another. This program is a good benchmark of where you should be two-three weeks from now in terms of complexity. Therefore if you can't solve this, please don't be too hard on yourself. If you can solve this, you should be damn proud of yourself because this is something I'd throw an intro CS student three weeks into learning how to use arrays and hashes. As is, I'd expect this to take you about ten days.