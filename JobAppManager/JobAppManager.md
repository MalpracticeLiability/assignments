# Assignment: Job Application Manager

Congrats! You've successfully completed Ada! Now it's time to apply for jobs!

The job application process can be overwhelming. At any one time, you can be juggling up to a dozen or so apps at the same time. It's easy to get lose track of deadlines in the chaos. Therefore one of the _biggest_ things I can ever advise a job applicant in tech is to keep track of all their apps. It's especially important to know who you're applying to, for what position, and at what date. That way you don't accidentally apply to the same job twice or you don't forget to follow up on your app.

## Primary Requirements
1. Create a Job Application Manager program that accepts the following _input_
    - How many applications you have to enter
    - What company you're applying to
    - What the position you're applying for is
    - What date you submitted the application
    
2. Have the Job Application Manager program calculate and _output_ the following:
    - The list of all your applications sorted by the date you applied, from earliest to latest
    
3. Follow Best Practices
    - Comment each section of code with what their functions, inputs, and outputs are
    - Comment on what each data structure (e.g. Array, Hash, etc.) should contain
    - No magic numbers
    
4. Keep interview questions in mind while you're coding. Prepare answers once finished coding
    - Describe something that you tride that did not work
    - How did you eventually fix that problem?
    - Describe a situation where your code may not perform as well as you'd like (i.e. how can someone break it?)
    
    
## Example Output
Below is an example program output. User input is indicated in ~~ (tildes).

```    

This is Molly's being a responsible job applicant!

Please enter how many job applications you have to sort through: ~8~

Enter the company for Application #1: ~Microsoft~
    Enter the position for Application #1: ~Full Stack Web Developer - Bing.com~
    Enter the month you submitted Application #1: ~4~
    Enter the day you submitted Application #1: ~22~

Enter the company for Application #2: ~Facebook~
    Enter the position for Application #2: ~Front End Dev - Messenger~
    Enter the month you submitted Application #2: ~5~
    Enter the day you submitted Application #2: ~12~
    
Enter the company for Application #3: ~Zillow~
    Enter the position for Application #3: ~Product - Full Stack~
    Enter the month you submitted Application #3: ~4~
    Enter the day you submitted Application #3: ~16~
    
Enter the company for Application #4: ~Amazon~
    Enter the position for Application #4: ~Amazon Video - Full Stack Developer~
    Enter the month you submitted Application #4: ~6~
    Enter the day you submitted Application #4: ~1~

Enter the company for Application #5: ~LinkedIn~
    Enter the position for Application #5: ~Full Stack Web Developer~
    Enter the month you submitted Application #5: ~3~
    Enter the day you submitted Application #5: ~20~
    
Enter the company for Application #6: ~Google~
    Enter the position for Application #6: ~Back End Developer - Google Maps~
    Enter the month you submitted Application #6: ~5~
    Enter the day you submitted Application #6: ~10~
    
Enter the company for Application #7: ~Apple~
    Enter the position for Application #7: ~Web Store Full Stack~
    Enter the month you submitted Application #7: ~5~
    Enter the day you submitted Application #7: ~2~
    
Enter the company for Application #8: ~Microsoft~
    Enter the position for Application #8: ~Front End Developer - Azure~
    Enter the month you submitted Application #8: ~5~
    Enter the day you submitted Application #8: ~28~
    


Sorting your applications by date:
    1. LinkedIn - Full Stack Web Developer - 3/20
    2. Zillow - Product - Full Stack - 4/16
    3. Microsoft -Full Stack Web Dev - Bing.com - 4/22
    4. Apple - Web Store Full Stack - 5/2
    5. Google - Back End Developer - Google Maps - 5/10
    6. Facebook - Front End Dev - Messenger - 5/12
    7. Microsoft - Front End Developer - Azure - 5/28
    8. Amazon - Amazon Video - Full Stack Developer - 6/1
    
Good luck on apps!
```

## Optional Enhancements
- Accept the name of the month as input (August, October, Februrary, etc.) and internally translate them to the numerical month (8, 10, 2, etc.)