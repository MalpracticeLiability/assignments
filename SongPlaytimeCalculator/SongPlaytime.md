# Assignment: Song Playtime Calculator
You are going to create a program to record what songs were played and for how long

## Primary Requirements
1. Create a song playtime program that accepts the following _input_ from the user for four songs:
	- The song name
	- Total length of the song (in seconds)
	- Number of times the song was played

2. Have the song playtime program calculate and _output_ the following
	- A summary of each song's title and total time (in seconds) the song was played
	- The total listening time of all four songs
	- Which of the four songs was listened to the most

3. Follow best practices
	- Comment on distinct sections of code to describe their function. See http://www.jsrepl.com/GZn6/13 for examples

4. Keep interview questions in mind while you're coding. Prepare answers once finished coding
	- Describe something that you tride that did not work
	- How did you eventually fix that problem?
	- Describe a situation where your code may not perform as well as you'd like (i.e. how can someone break it?)

## Example Output
Below is an example program output. User input is indicated in ~~ (tildes).

```

Welcome to the Benji's Cool Time Summer Jamz Mix control panel
Use this to find out which of Benji's Cool Time Summer Jamz was played the most

Song Name #1: ~Shoop - Salt n Peppa~
	Playtime (in seconds): ~249~
	Number of times played: ~5~

Song Name #2: ~Pop Song 89 - R.E.M.~
	Playtime (in seconds): ~182~
	Number of times played: ~2~

Song Name #3: ~Smooth as Silver - Duke Silver~
	Playtime (in seconds): ~150~
	Number of times played: ~3~

Song Name #4: ~5,000 Candles in the Wind - Mouse Rat~
	Playtime (in seconds): ~618~
	Number of times played: ~8~

Playtimes:
	Shoop - Salt n Peppa: 1245 
	Pop Song 89 - R.E.M.: 364 
	Smooth as Silver - Duke Silver: 450
	5,000 Candles in the Wind - Mouse Rat: 4944

=========================================================
	Total Playtime: 7003

Most popular song: 5,000 Candles in the Wind - Mouse Rat with 4944 seconds!
```

## Optional Enhancements
- Let the user dictate how many songs to input, rather than hardcode to 4 songs (EASY)
- Don't use if/elsif/else statements to find the song with the most playtime (MEDIUM)
- Take song playtime in seconds, but output song playtimes in mm:ss format (MEDIUM)
- Store song and artist separately, then output if there are multiple songs from same artist (HARD)
