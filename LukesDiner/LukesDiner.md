# Assignment: Luke's Diner
It's Single's Day (1/1) in Stars Hollow! Luke fortunately hit his head the night before while cleaning up, and he decided to take part in the town event!

To celebrate Single's Day, Luke is making only one of everything on his menu. Once an item is sold, that item is not available for the rest of the day. Your job is to write a food ordering program that lets Luke enter the menu in the morning, keeps track of what items are still available on the menu, and removes items from the menu as they're ordered. At the end of the day, print out what items were sold and the total money Luke made.


## Primary Requirements

1. Create a menu management program that accepts the following _input_ from the user:
    -    The complete menu, including item + price, when the program starts. The program should ask for each item + price individually and prompt the user if they have more input. 
    -    What item the customer would like to order. The program should ask for each order individually and prompt the user if they have more orders to enter.
    
2. Have the menu management program calculate and _output_ the following:
    -    The menu items still available for each order
    -    After all orders are entered, what items were sold and how much money Luke made throughout the day
    
3. Follow Best Practices
    -    Comment each section of code with what their functions, inputs, and outputs are
    -    All money outputs should be properly formatted with commas, decimal points, and dollar signs.
    -    Comment on what each data structure (e.g. Array, Hash, etc.) should contain
    -    No magic numbers
    
4. Keep interview questions in mind while you're coding. Prepare answers once finished coding
	- Describe something that you tride that did not work
	- How did you eventually fix that problem?
	- Describe a situation where your code may not perform as well as you'd like (i.e. how can someone break it?)
    
    
    
## Example Output
Below is an example program output. User input is indicated in ~~ (tildes).

```

Welcome to Luke's Diner. No cell phones allowed inside. Taylor pays double.

##Menu Entry##
First, please enter each item on the menu:

Item: ~Coffee~
Price: ~1.50~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Cheeseburger~
Price: ~$4.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Eggs and Bacon~
Price: ~8.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Pancakes~
Price: ~5.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Eggs and Sausage~
Price: ~10.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Country Fried Steak and Eggs~
Price: ~10.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Steak~
Price: ~12.00~
    Do you have more items to enter (y/n): ~y~
    
Item: ~Iced Mocha in a Mason Jar~
Price: ~3.50~
    Do you have more items to enter (y/n): ~n~
    
##End Menu Entry##



##Begin Ordering##

Available Items:
    
1. Coffee		1.50
2. Cheeseburger		4.00
3. Eggs and Bacon		8.00
4. Pancakes		5.00
5. Eggs and Sausage		10.00
6. Country Fried Steak and Eggs		10.00
7. Steak		12.00
8. Iced Mocha in a Mason Jar		3.50

Order Number: ~7~
Steak Ordered. Do you have more orders to enter? (y/n): ~y~

Available Items:

1. Coffee		1.50
2. Cheeseburger		4.00
3. Eggs and Bacon		8.00
4. Pancakes		5.00
5. Eggs and Sausage		10.00
6. Country Fried Steak and Eggs		10.00
8. Iced Mocha in a Mason Jar		3.50

Order Number: ~1~
Coffee Ordered. Do you have more orders to enter? (y/n) ~y~

Available Items:

2. Cheeseburger		4.00
3. Eggs and Bacon		8.00
4. Pancakes		5.00
5. Eggs and Sausage		10.00
6. Country Fried Steak and Eggs		10.00
7. Steak		12.00
8. Iced Mocha in a Mason Jar		3.50

Order Number: ~8~
Iced Mocha in a Mason Jar ordered. Do you have more orders to enter? (y/n) ~n~



##Daily Summary##

Items sold:
    - Steak        $12.00
    - Coffee        $1.50
    - Iced Mocha in a Mason Jar        $3.50
    
Total money made: $17.00

```

## Optional Enhancements

- Store money as an integer of cents rather than a float of dollars
- Have money expressed to two decimal places in every output.
