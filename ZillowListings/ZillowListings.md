# Assignment: Zillow Listings

Surprise! I've actually been working for Zillow this whole time, and now I'm getting you to do my work for me! 

You will need to write a program that prompts the user to enter the address and price of available listings. After all the listings are updated, the program will then ask the user what their max budget is, and will display all houses that cost less than or equal to that budget. I will need this one by Monday please otherwise they'll probably fire me and will make fun of me behind my back.

## Primary Requirements
1. Create a house listings that accepts the following _input_ from the user:
    - The address of a house
    - The price of a house
    - Whether or not the user has more listings to enter
    - What the max budget of their buyer is
2. Have the house listings program calculate and _output_ the following:
    - A list of all houses that cost less than or equal to the max budget
3. Follow Best Practices
    - Comment each section of code with what their functions, inputs, and outputs are
    - All money outputs should be properly formatted with dollar signs and commas
    - Comment on what each data structure (e.g. Array, Hash, etc.) should contain
    - No magic numbers
4. Keep interview questions in mind while you're coding. Prepare answers once finished coding
    - Describe something that you tride that did not work
    - How did you eventually fix that problem?
    - Describe a situation where your code may not perform as well as you'd like (i.e. how can someone break it?)
    
## Example Output
Below is an example program output. User input is indicated in ~~ (tildes).

```

This is Molly's attempt to save Daniel's job!


##House Listings Entry##

Address of House #1: ~123 Main St.~
Price of House #1: ~500000~
    Do you have more houses to enter? (y/n) ~y~

Address of House #2: ~4567 Foo Ave.~
Price of House #2: ~250000~
    Do you have more houses to enter? (y/n) ~y~
    
Address of House #3: ~42 Bar Blvd~
Price of House #3: ~350000~
    Do you have more houses to enter? (y/n) ~y~
    
Address of House #4: ~12 Baz Road~
Price of House #4: ~299999~
    Do you have more houses to enter? (y/n) ~y~

Address of House #5: ~23 Qux Court~
Price of House #5: 175000
    Do you have more houses to enter? (y/n) ~n~
    
    
    

##House Finder##

What is your max budget? ~300000~

Listings that match your budget:
    - 4567 Foo Ave        $250,000
    - 12 Baz Road        $299,999
    - 23 Qux Court        $175,000
    
Enjoy your new home!
    
```

## Optional Enhancements

- Store money as an integer in terms of cents rather than a float in terms of dollars. This is a much better way to store money for reasons I'll get into later, but for now try practicing storing money as an int of cents.


